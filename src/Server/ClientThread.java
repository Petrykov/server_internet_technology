package Server;

import DataProdvider.DataProvider;
import Group.Group;
import Messages.Messages;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class ClientThread implements Runnable {

    private Socket socket;
    private String username;
    private String menu = " ";
    private String server_message;
    private String server_message_split;
    private String target_message = " ";

    private int amount_of_users;
    private boolean isConnected = true;
    public boolean pong_received;

    private List<ClientThread> connections;

    private DataProvider dataProvider = DataProvider.getInstance();
    private BufferedReader bf;
    private PrintWriter writer;
    private Group joined_group;
    private Messages clMsg = null;

    public ClientThread(Socket clientThread, List<ClientThread> connections) {
        this.connections = connections;
        this.socket = clientThread;
        this.username = " ";
    }

    @Override
    public void run() {

        PingClient pingClient = new PingClient(this);
        new Thread(pingClient).start();

        try {
            InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            bf = new BufferedReader(new InputStreamReader(is));
            writer = new PrintWriter(os);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String hello = writeToClient("HELO. Welcome to our chat");
        System.out.println("<< " + hello);

        try {
            while (this.isConnected) {
                server_message = bf.readLine();

                if (server_message != null) {
                    System.out.println(">> [" + username + "] " + server_message);
                    clMsg = new Messages(server_message);
                    Messages.messageType clMsgType = clMsg.messageType();

                    switch (clMsgType) {

                        case HELO:
                            caseHelo();
                            break;

                        case GET_USERS:
                            caseGetUsers();
                            break;

                        case DIRECT_MESSAGE:
                            caseDirectMessage();
                            break;

                        case CREATE_GROUP:
                            caseCreateGroup();
                            break;

                        case GET_GROUPS:
                            caseGetGroups();
                            break;

                        case JOIN_GROUP:
                            caseJoinGroup();
                            break;

                        case BCST:
                            caseBcst();
                            break;

                        case KICK:
                            caseKick();
                            break;

                        case QUIT:
                            caseQuit();
                            break;

                        case PONG:
                            pong_received = true;
                            break;

                        case FILE_TRANSFER:
                            caseFileTransfer();
                            break;

                        case KEY:
                            caseKey();
                            break;

                        case GET_KEY:
                            caseGetKey();
                            break;
                    }
                }
            }
        } catch (IOException e) {
            isConnected = false;
            e.printStackTrace();
        }
    }

    private void caseHelo() {
        boolean user_exists = false;

        for (ClientThread cl : connections) {
            if (clMsg.getMessage(1).equals(cl.getUsername())) {
                String already_logged_in = writeToClient("-ERR user already logged in");
                System.out.println("<< [" + username + "] " + already_logged_in);
                user_exists = true;
                break;
            }
        }

        if (!user_exists) {
            username = clMsg.getMessage(1);
            menu = writeToClient("MENU");
            System.out.println("<< [" + username + "] " + menu);
        }
    }

    private void caseGetUsers() {
        StringBuilder users = new StringBuilder(" ");
        for (ClientThread connection : connections) {
            this.amount_of_users++;
            if (!connection.getUsername().equals(this.username)) {
                users.append(" \n- ").append(connection.getUsername());
            }
        }

        if (!users.toString().equals(" ")) {
            writeToClient("Users online (" + (amount_of_users - 1) + ") :" + users);
            System.out.println("<< [" + username + "] Users online: (" + (amount_of_users - 1) + ")");

        } else {
            String no_users_online = writeToClient("No users online");
            System.out.println("<< [" + username + "] " + no_users_online);
        }

        this.amount_of_users = 0;

        writeToClient("MENU");
        System.out.println("<< [" + username + "] " + menu);
    }

    private void caseDirectMessage() {
        boolean user_is_found = false;
        server_message_split = clMsg.getMessage(1);

        for (ClientThread connection : connections) {
            if (connection.getUsername().equals(target_message)) {
                connection.writePrivateMessage(this.username, clMsg.getMessage(1));
                System.out.println("<< [" + username + "] [" + username + "] ---> [" + connection.getUsername() + "] direct_msg: ");
                user_is_found = true;
            }
        }

        if (!user_is_found) {
            String no_such_user = writeToClient("Sorry there's no such user");
            System.out.println("<< [" + username + "] " + no_such_user);
        }

        writeToClient("MENU");
        System.out.println("<< [" + username + "] " + menu);
    }

    private void caseCreateGroup() {
        boolean already_created = false;

        Group group = new Group(this, clMsg.getMessage(0));

        for (Group g : dataProvider.getGroups()) {
            if (g.getName().equals(group.getName())) {
                already_created = true;
                String already_exists = writeToClient("There's already a group with such name");
                System.out.println("<< [" + username + "] " + already_exists);
                writeToClient("MENU ");
                System.out.println("<< [" + username + "] " + menu);
            }
        }

        if (!already_created) {
            dataProvider.addGroup(group);
            String group_added = writeToClient("Group was successfully added");
            System.out.println("<< [" + username + "] " + group_added);
            writeToClient("MENU");
            System.out.println("<< [" + username + "] " + menu);
        }
    }

    private void caseGetGroups() {
        String groups = dataProvider.getGroupsName();
        if (groups.isEmpty()) {
            writeToClient("Groups are not available right now ");
            System.out.println("<< [" + username + "] NO_GROUPS");
        } else {
            writeToClient(groups);
            System.out.println("<< [" + username + "] SEND_GROUPS");
        }

        writeToClient("MENU");
        System.out.println("<< [" + username + "] " + menu);
    }

    private void caseJoinGroup() {
        boolean group_is_found = false;

        server_message_split = clMsg.getMessage(0);

        for (int i = 0; i < dataProvider.getGroups().size(); i++) {
            if (dataProvider.getGroups().get(i).getName().equals(server_message_split)) {
                joined_group = dataProvider.getGroups().get(i);
                dataProvider.getGroups().get(i).addClient(this);
                group_is_found = true;
                String bcst = writeToClient("+OK BCST");
                System.out.println("<< [" + username + "] " + bcst);
            }
        }

        if (!group_is_found) {
            String no_such_group = writeToClient("There's no such group with the name you're looking for");
            System.out.println("<< [" + username + "] " + no_such_group);
            writeToClient("<< MENU ");
            System.out.println("<< [" + username + "] " + menu);
        }
    }

    private void caseBcst() {
        server_message_split = clMsg.getMessage(0);

        for (int i = 0; i < joined_group.getSize(); i++) {
            joined_group.getClients().get(i).writeToClient("+OK BCST From (" + username + ") msg in group: " + server_message_split);
            System.out.println("<< [" + username + "] +OK BCST [" + username + "] msg" + server_message_split);
        }
    }

    private void caseKick() {

        boolean client_to_kick = false;

        server_message_split = clMsg.getMessage(1);

        if (joined_group.getOwner().equals(this)) {
            for (int i = 0; i < joined_group.getSize(); i++) {
                if (joined_group.getClients().get(i).getUsername().equals(server_message_split)) {
                    joined_group.getClients().get(i).writeToClient("KICKED");
                    joined_group.removeClient(joined_group.getClients().get(i));
                    String user_kicked = writeToClient("Member " + server_message_split + " was kicked");
                    System.out.println("<< [" + username + "] " + user_kicked);
                    client_to_kick = true;
                }
            }
        } else {
            client_to_kick = true;
            String no_permissions = writeToClient("You don't have permissions to kick members");
            System.out.println("<< [" + username + "] " + no_permissions);
        }

        if (!client_to_kick) {
            String user_not_found = writeToClient("There's no such user in group you want to kick");
            System.out.println("<< [" + username + "] " + user_not_found);

        }

        String bcst = writeToClient("+OK BCST");
        System.out.println("<< [" + username + "] " + bcst);
    }

    private void caseQuit() {
        dataProvider.quitGroup(joined_group, this);
        joined_group.removeClient(this);
        writeToClient("MENU");
        System.out.println("<< [" + username + "] " + menu);
    }

    private void caseFileTransfer() throws IOException {
        boolean user_is_found = false;

        String[] file_transfer = server_message.split(" ");

        for (ClientThread connection : connections) {
            if (connection.getUsername().equals(file_transfer[1])) {
                int size = Integer.parseInt(file_transfer[2]);
                System.out.println("file size is: " + size);
                byte[] file = new byte[size];
                socket.getInputStream().read(file, 0, file.length);

                for (byte b : file) {
                    System.out.print(" " + b);
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                connection.writeToClient("FILE_TRANSFER " + size);
                connection.sendFile(file);
                System.out.println("<< [" + username + "] [" + username + "] ---> [" + connection.getUsername() + "] send file request ");
                user_is_found = true;
            }
        }

        if (!user_is_found) {
            String no_such_user = writeToClient("Sorry there's no such user");
            System.out.println("<< [" + username + "] " + no_such_user);
        }

        writeToClient("MENU");
        System.out.println("<< [" + username + "] " + menu);
    }

    private void caseKey() {
        server_message_split = clMsg.getMessage(1);
        System.out.println(server_message_split);
        dataProvider.addPublicKeyToDB(username, server_message_split);
    }

    private void caseGetKey() {
        server_message_split = clMsg.getMessage(1);

        target_message = server_message_split;
        System.out.println("target message: " + target_message);
        String receiver_public_key = dataProvider.getKey(target_message);
        if(receiver_public_key.isEmpty()){
            writeToClient("There's no such user you're looking for " + receiver_public_key);
            System.out.println("<< [" + username + "] Client not found");
            writeToClient("MENU");
            System.out.println("<< [" + username + "] " + menu);
        }else{
            writeToClient("PUBLIC_KEY " + receiver_public_key);
        }
    }

    public String getUsername() {
        return username;
    }

    public List<ClientThread> getConnections() {
        return connections;
    }

    public void writePrivateMessage(String sender, String message) {
        writer.println("PRIVATE_MESSAGE From user: (" + sender + ") msg: " + message);
        writer.flush();
    }

    public String writeToClient(String message) {
        writer.println(message);
        writer.flush();

        return message;
    }

    public void sendFile(byte[] file) {
        try {
            socket.getOutputStream().write(file, 0, file.length);
            socket.getOutputStream().flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void timeOutConnection() {
        try {
            System.err.println("[DROP CONNECTION] " + username);
            connections.remove(this);
            socket.close();

        } catch (Exception e) {
            System.out.println("Exception while closing output stream: " + e.getMessage());
        }
        this.isConnected = false;
    }

}