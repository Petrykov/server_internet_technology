package Server;

public class PingClient implements Runnable {

    private ClientThread clientThread;
    private boolean send_ping;

    PingClient(ClientThread clientThread) {
        this.send_ping = true;
        this.clientThread = clientThread;
    }

    public void run() {

        while (send_ping) {
            try {
                Thread.sleep(5000);
                clientThread.pong_received = false;
                clientThread.writeToClient("PING " + clientThread.getUsername());
                System.out.println("<< ["+clientThread.getUsername()+"] PING");
                Thread.sleep(5000L);

                if (clientThread.pong_received) {
                    continue;
                }

                send_ping = false;
                clientThread.timeOutConnection();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
