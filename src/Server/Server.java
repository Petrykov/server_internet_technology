package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Server {

    private List<ClientThread> connections = new ArrayList<>();
    private ServerSocket serverSocket = null;
    public static void main(String[] args) {
        new Server().run();
    }

    public void run() {

        try {
            serverSocket = new ServerSocket(1339);
        } catch (IOException e) {
            e.printStackTrace();
        }

        while (true) {
            try {
                Socket socket = serverSocket.accept();
                ClientThread userConnection = new ClientThread(socket, connections);
                connections = userConnection.getConnections();
                connections.add(userConnection);
                new Thread(userConnection).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
