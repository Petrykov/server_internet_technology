package DataProdvider;

import Group.Group;
import Server.ClientThread;

import java.util.*;

import static java.util.Map.*;

public class DataProvider {

    private static DataProvider instance;
    private List<Group> groups;
    private static Map<String, String> publicKeys;

    static {
        instance = new DataProvider();
        publicKeys = new HashMap<>();
    }

    public void addPublicKeyToDB(String key, String name) {
        publicKeys.put(key, name);
    }

    public static DataProvider getInstance() {
        return instance;
    }

    public DataProvider() {
        groups = new ArrayList<>();
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void addGroup(Group group) {
        this.groups.add(group);
    }

    public void quitGroup(Group group, ClientThread client) {
        for (Group g : groups) {
            if (g.equals(group)) {
                g.removeClient(client);
            }
        }
    }

    public String getGroupsName() {
        StringBuilder to_return = new StringBuilder();

        if (groups.size() != 0) {
            for (int i = 0; i < groups.size(); i++) {
                if (to_return.length() == 0) {
                    to_return.append(i + 1).append(")").append(groups.get(i).getName());
                } else {
                    to_return.append("\n").append(i + 1).append(")").append(groups.get(i).getName());
                }
            }
        } else {
            to_return = new StringBuilder("Sorry, no groups are available right now");
        }

        return to_return.toString();
    }

    public String   getKey(String username) {
        String to_return = "";

        for (Entry<String, String> pair : publicKeys.entrySet()) {
            System.out.println(pair.getKey() + " - " + pair.getValue());
            if (pair.getKey().equals(username)) {
                to_return = pair.getValue();
            }
        }
        System.out.println("to return is : " + to_return);
        return to_return;
    }
}
