package Messages;

public class Messages {

    public enum messageType {
        HELO,
        PING,
        UNKNOWN,
        PONG,
        GET_USERS,
        DIRECT_MESSAGE,
        CREATE_GROUP,
        GET_GROUPS,
        JOIN_GROUP,
        BCST,
        QUIT,
        KICK,
        FILE_TRANSFER,
        KEY,
        GET_KEY
    }

    private String message;

    public Messages(String message) {
        this.message = message;
    }

    public messageType messageType() {

        if (message.startsWith("HELO")) {
            return messageType.HELO;
        } else if (message.startsWith("PING")) {
            return messageType.PING;
        } else if (message.startsWith("PONG")) {
            return messageType.PONG;
        } else if (message.startsWith("GET_USERS")) {
            return messageType.GET_USERS;
        } else if (message.startsWith("DIRECT_MESSAGE")) {
            return messageType.DIRECT_MESSAGE;
        } else if (message.startsWith("CREATE_GROUP")) {
            return messageType.CREATE_GROUP;
        } else if (message.startsWith("GET_GROUPS")) {
            return messageType.GET_GROUPS;
        } else if (message.startsWith("JOIN_GROUP")) {
            return messageType.JOIN_GROUP;
        } else if (message.startsWith("BCST")) {
            return messageType.BCST;
        } else if (message.startsWith("QUIT")) {
            return messageType.QUIT;
        } else if (message.startsWith("KICK")) {
            return messageType.KICK;
        } else if(message.startsWith("FILE_TRANSFER")){
            return messageType.FILE_TRANSFER;
        }else if(message.startsWith("KEY")){
            return messageType.KEY;
        }else if(message.startsWith("GET_KEY")){
            return messageType.GET_KEY;
        }

        return messageType.UNKNOWN;
    }

    public String getMessage(int position) {
        StringBuilder to_return = new StringBuilder(" ");
        String[] input_array = message.split(" ");;

        switch (position) {

            // name
            case 1:
                if(input_array.length>1){
                    to_return = new StringBuilder(input_array[1]);
                }
                break;

            // message
            case 2:
                input_array = message.split(" ");
                for (int i = 2; i < input_array.length; i++) {
                    to_return.append(input_array[i]).append(" ");
                }
                break;

            // group
            case 0:

                input_array = message.split(" ");
                for (int i = 1; i < input_array.length; i++) {
                    to_return.append(input_array[i]).append(" ");
                }

                break;
        }

        return to_return.toString();
    }
}
