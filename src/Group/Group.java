package Group;

import Server.ClientThread;

import java.util.ArrayList;
import java.util.List;

public class Group {

    private List<ClientThread> clients;

    private ClientThread owner;
    private String name;

    public Group(ClientThread owner, String name){
        this.owner = owner;
        this.name = name;
        clients = new ArrayList<>();
    }

    public int getSize(){
        return clients.size();
    }

    public String getName() {
        return name;
    }

    public void addClient(ClientThread clientThread){
        clients.add(clientThread);
    }

    public void removeClient(ClientThread clientThread){
        clients.remove(clientThread);
    }

    public List<ClientThread> getClients() {
        return clients;
    }

    public ClientThread getOwner() {
        return owner;
    }
}
